package com.utils {
public class ColourUtils {
    public function ColourUtils() {
    }

    public static function returnARGB(rgb:uint, alpha:Number):uint {
        //newAlpha has to be in the 0 to 255 range
        var newAlpha:uint = Math.round(alpha * 255);
        var argb:uint = 0;
        argb += (newAlpha << 24);
        argb += (rgb);
        return argb;
    }

    public static function getNumberAsHexString(number:uint, minimumLength:uint = 1):String {
        // The string that will be output at the end of the function.
        var string:String = number.toString(0x10).toUpperCase();

        // While the minimumLength argument is higher than the length of the string, add a leading zero.
        while (minimumLength > string.length) {
            string = "0" + string;
        }

        // Return the result with a "0x" in front of the result.
        return "0x" + string;
    }
}
}