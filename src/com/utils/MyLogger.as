package com.utils {

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;

public class MyLogger {
    public function MyLogger():void {
    }

    public static function log(clazz:*):ILogger {
        return getLogger(clazz);
    }
}
}